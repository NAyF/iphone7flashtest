//
//  Utilities.swift
//  iPhone7FlashTest
//
//  Created by NAyF Walker on 07/10/2016.
//  Copyright © 2016 NAyF. All rights reserved.
//

import UIKit

public var CGPointOne: CGPoint { return CGPoint(x: 1, y: 1) }

extension CGSize {
    func gradientImage(colors: [UIColor]) -> UIImage {
        return gradientImage(colors, startPoint: CGPointZero, endPoint: CGPointOne)
    }
    func gradientImage(colors: [UIColor], startPoint: CGPoint, endPoint: CGPoint) -> UIImage {
        var cgColors = [CGColor]()
        for color in colors {
            cgColors.append(color.CGColor)
        }
        if cgColors.count == 1 {
            cgColors.append(colors[0].CGColor)
        }
        
        let startPointInFrame = CGPoint(x: width * startPoint.x, y: height * startPoint.y)
        let endPointInFrame = CGPoint(x: width * endPoint.x, y: height * endPoint.y)
        
        var contextSize = self
        if contextSize.width == 0 {
            contextSize.width = 1
        }
        if contextSize.height == 0 {
            contextSize.height = 1
        }
        UIGraphicsBeginImageContextWithOptions(contextSize, false, 0)
        let context = UIGraphicsGetCurrentContext()!
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradientCreateWithColors(colorSpace, cgColors, nil)
        
        let drawingOptions: CGGradientDrawingOptions = [.DrawsBeforeStartLocation, .DrawsAfterEndLocation]
        CGContextDrawLinearGradient(context, gradient!, startPointInFrame, endPointInFrame, drawingOptions)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}

extension UIView {
    func gradientImage(colors: [UIColor]) -> UIImage {
        return gradientImage(colors, startPoint: CGPointZero, endPoint: CGPointOne)
    }
    
    func gradientImage(colors: [UIColor], startPoint: CGPoint, endPoint: CGPoint) -> UIImage {
        return frame.size.gradientImage(colors, startPoint: startPoint, endPoint: endPoint)
    }
}

//
//  LaunchScreenBridge.swift
//  iPhone7FlashTest
//
//  Created by NAyF Walker on 07/10/2016.
//  Copyright © 2016 NAyF. All rights reserved.
//

import UIKit

class LaunchScreenBridge: UIViewController {
    
    @IBOutlet weak var redView: UIView?
    
    let purpleView = UIView()
    var loadCompletionHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        purpleView.frame = redView!.frame
        purpleView.backgroundColor = .purpleColor()
        purpleView.alpha = 0
        view.addSubview(purpleView)
    
        UIView.animateWithDuration(0.5, delay: 0.5, options: .CurveEaseInOut, animations: {
            
            self.purpleView.alpha = 1
            
            }) { (completed) in
                
                self.redView?.removeFromSuperview()
                
                let duration = NSTimeInterval(1)
                let angle = CGFloat(M_PI)
                let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                CATransaction.begin()
                let rotation = CABasicAnimation(keyPath: "transform.rotation.z")
                rotation.byValue = angle
                rotation.duration = duration
                rotation.timingFunction = timingFunction
                rotation.removedOnCompletion = false
                CATransaction.setCompletionBlock({
                    self.purpleView.transform = CGAffineTransformRotate(self.purpleView.transform, angle)
                    self.loadCompletionHandler?()
                })
                
                self.purpleView.layer.addAnimation(rotation, forKey: "rotationAnimation")
                CATransaction.commit()
        }
    }
}

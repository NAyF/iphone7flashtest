//
//  SecondViewController.swift
//  iPhone7FlashTest
//
//  Created by NAyF Walker on 07/10/2016.
//  Copyright © 2016 NAyF. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    let maskedBlurView = UIVisualEffectView(effect: UIBlurEffect(style: .Dark))
    let dismissButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .purpleColor()
        
        let maskedBlurViewHeight = CGFloat(180)
        let maskedBlurViewFrame = CGRect(x: 0, y: view.bounds.height - maskedBlurViewHeight, width: view.bounds.width, height: maskedBlurViewHeight)
        
        maskedBlurView.frame = maskedBlurViewFrame
        
        let gradientedMaskView = UIView(frame: maskedBlurView.bounds)
        gradientedMaskView.backgroundColor = UIColor(patternImage: maskedBlurView.gradientImage([.clearColor(), .blackColor()], startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 0.5)))
        
        maskedBlurView.maskView = gradientedMaskView
        maskedBlurView.userInteractionEnabled = false
        view.addSubview(maskedBlurView)
        
        let dismissButtonWidth = CGFloat(150)
        let halfDismissButtonWidth = dismissButtonWidth * 0.5
        let halfViewWidth = view.bounds.width * 0.5
        dismissButton.frame = CGRect(x: halfViewWidth - halfDismissButtonWidth, y: maskedBlurView.center.y - 20, width: dismissButtonWidth, height: 30)
        dismissButton.setTitle("Dismiss", forState: .Normal)
        dismissButton.addTarget(self, action: #selector(SecondViewController.dismissButtonTapped), forControlEvents: .TouchUpInside)
        view.addSubview(dismissButton)
    }
    
    func dismissButtonTapped() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}

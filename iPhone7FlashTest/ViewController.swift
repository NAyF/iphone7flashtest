//
//  ViewController.swift
//  iPhone7FlashTest
//
//  Created by NAyF Walker on 07/10/2016.
//  Copyright © 2016 NAyF. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let maskedBlurView = UIVisualEffectView(effect: UIBlurEffect(style: .Dark))
    let presentButton = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .blueColor()
        
        let maskedBlurViewHeight = CGFloat(180)
        let maskedBlurViewFrame = CGRect(x: 0, y: view.bounds.height - maskedBlurViewHeight, width: view.bounds.width, height: maskedBlurViewHeight)
        
        maskedBlurView.frame = maskedBlurViewFrame
        
        let gradientedMaskView = UIView(frame: maskedBlurView.bounds)
        gradientedMaskView.backgroundColor = UIColor(patternImage: maskedBlurView.gradientImage([.clearColor(), .blackColor()], startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 0.5)))
        
        maskedBlurView.maskView = gradientedMaskView
        maskedBlurView.userInteractionEnabled = false
        view.addSubview(maskedBlurView)
        
        let presentButtonWidth = CGFloat(300)
        let halfPresentButtonWidth = presentButtonWidth * 0.5
        let halfViewWidth = view.bounds.width * 0.5
        presentButton.frame = CGRect(x: halfViewWidth - halfPresentButtonWidth, y: maskedBlurView.center.y - 20, width: presentButtonWidth, height: 30)
        presentButton.setTitle("Present second view controller", forState: .Normal)
        presentButton.addTarget(self, action: #selector(ViewController.presentButtonTapped), forControlEvents: .TouchUpInside)
        view.addSubview(presentButton)
    }
    
    func presentButtonTapped() {
        let secondViewController = SecondViewController()
        secondViewController.modalPresentationStyle = .OverCurrentContext
        secondViewController.modalTransitionStyle = .CrossDissolve
        presentViewController(secondViewController, animated: true, completion: nil)
    }

}


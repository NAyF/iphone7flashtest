//
//  AppDelegate.swift
//  iPhone7FlashTest
//
//  Created by NAyF Walker on 07/10/2016.
//  Copyright © 2016 NAyF. All rights reserved.
//

import UIKit

let useNavigationController = false

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CAAnimationDelegate {
    
    var window: UIWindow?
    let viewController = ViewController()
    var navigationController: UINavigationController?
    
    func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if !useNavigationController {
            self.viewController.view.removeFromSuperview()
            window?.rootViewController = self.viewController
        }
    }
    
    func applicationDidFinishLaunching(application: UIApplication) {
        let storyboard = UIStoryboard(name: "LaunchScreenBridge", bundle: nil)
        let launchScreenBridge = storyboard.instantiateInitialViewController() as! LaunchScreenBridge
        
        launchScreenBridge.modalTransitionStyle = .CrossDissolve
        launchScreenBridge.loadCompletionHandler = {
            if useNavigationController {
                self.viewController.modalPresentationStyle = .FullScreen
                self.viewController.modalTransitionStyle = .CrossDissolve
                launchScreenBridge.presentViewController(self.viewController, animated: true, completion: nil)
                
            } else {
                let animation = CABasicAnimation(keyPath: "opacity")
                animation.duration = 0.35
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                animation.fromValue = 0
                animation.toValue = 1
                animation.delegate = self
                self.viewController.view.layer.addAnimation(animation, forKey: nil)
                launchScreenBridge.view.addSubview(self.viewController.view)
            }
        }
        
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        if useNavigationController {
            navigationController = UINavigationController(rootViewController: launchScreenBridge)
            navigationController?.navigationBarHidden = true
            window?.rootViewController = navigationController
        } else {
            window?.rootViewController = launchScreenBridge
        }
        
        window?.makeKeyAndVisible()
    }
}

